package com.example.assignment3.mappers;

import com.example.assignment3.model.Franchise;
import com.example.assignment3.model.Movie;
import com.example.assignment3.model.dto.movie.MovieDTO;
import com.example.assignment3.model.dto.movie.MoviePostDTO;
import java.util.ArrayList;
import java.util.Collection;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-02-03T14:40:58+0100",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 19.0.2 (Oracle Corporation)"
)
@Component
public class MovieMapperImpl extends MovieMapper {

    @Override
    public MovieDTO movieToMovieDto(Movie movie) {
        if ( movie == null ) {
            return null;
        }

        MovieDTO movieDTO = new MovieDTO();

        movieDTO.setFranchise( movieFranchiseId( movie ) );
        movieDTO.setCharacters( map( movie.getCharacters() ) );
        movieDTO.setId( movie.getId() );
        movieDTO.setTitle( movie.getTitle() );
        movieDTO.setGenre( movie.getGenre() );
        movieDTO.setReleaseYear( movie.getReleaseYear() );
        movieDTO.setDirector( movie.getDirector() );
        movieDTO.setPicture( movie.getPicture() );
        movieDTO.setTrailer( movie.getTrailer() );

        return movieDTO;
    }

    @Override
    public Collection<MovieDTO> movieToMovieDto(Collection<Movie> movies) {
        if ( movies == null ) {
            return null;
        }

        Collection<MovieDTO> collection = new ArrayList<MovieDTO>( movies.size() );
        for ( Movie movie : movies ) {
            collection.add( movieToMovieDto( movie ) );
        }

        return collection;
    }

    @Override
    public Movie moviePostDtoToMovie(MoviePostDTO moviePostDTO) {
        if ( moviePostDTO == null ) {
            return null;
        }

        Movie movie = new Movie();

        movie.setTitle( moviePostDTO.getTitle() );
        movie.setReleaseYear( moviePostDTO.getReleaseYear() );
        movie.setDirector( moviePostDTO.getDirector() );

        return movie;
    }

    private int movieFranchiseId(Movie movie) {
        if ( movie == null ) {
            return 0;
        }
        Franchise franchise = movie.getFranchise();
        if ( franchise == null ) {
            return 0;
        }
        int id = franchise.getId();
        return id;
    }
}
