package com.example.assignment3.mappers;

import com.example.assignment3.model.Character;
import com.example.assignment3.model.dto.character.CharacterDTO;
import com.example.assignment3.model.dto.character.CharacterPostDTO;
import java.util.ArrayList;
import java.util.Collection;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-02-03T14:40:57+0100",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 19.0.2 (Oracle Corporation)"
)
@Component
public class CharacterMapperImpl extends CharacterMapper {

    @Override
    public CharacterDTO characterToCharacterDto(Character character) {
        if ( character == null ) {
            return null;
        }

        CharacterDTO characterDTO = new CharacterDTO();

        characterDTO.setMovies( map( character.getMovies() ) );
        characterDTO.setId( character.getId() );
        characterDTO.setName( character.getName() );
        characterDTO.setAlias( character.getAlias() );
        characterDTO.setGender( character.getGender() );
        characterDTO.setPicture( character.getPicture() );

        return characterDTO;
    }

    @Override
    public Collection<CharacterDTO> characterToCharacterDto(Collection<Character> characters) {
        if ( characters == null ) {
            return null;
        }

        Collection<CharacterDTO> collection = new ArrayList<CharacterDTO>( characters.size() );
        for ( Character character : characters ) {
            collection.add( characterToCharacterDto( character ) );
        }

        return collection;
    }

    @Override
    public Character characterPostDtoToCharacter(CharacterPostDTO characterPostDTO) {
        if ( characterPostDTO == null ) {
            return null;
        }

        Character character = new Character();

        character.setName( characterPostDTO.getName() );
        character.setGender( characterPostDTO.getGender() );

        return character;
    }
}
