package com.example.assignment3.mappers;

import com.example.assignment3.model.Franchise;
import com.example.assignment3.model.dto.franchise.FranchiseDTO;
import com.example.assignment3.model.dto.franchise.FranchisePostDTO;
import java.util.ArrayList;
import java.util.Collection;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-02-03T14:40:58+0100",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 19.0.2 (Oracle Corporation)"
)
@Component
public class FranchiseMapperImpl extends FranchiseMapper {

    @Override
    public FranchiseDTO franchiseToFranchiseDto(Franchise franchise) {
        if ( franchise == null ) {
            return null;
        }

        FranchiseDTO franchiseDTO = new FranchiseDTO();

        franchiseDTO.setMovies( map( franchise.getMovies() ) );
        franchiseDTO.setId( franchise.getId() );
        franchiseDTO.setName( franchise.getName() );
        franchiseDTO.setDescription( franchise.getDescription() );

        return franchiseDTO;
    }

    @Override
    public Collection<FranchiseDTO> franchiseToFranchiseDto(Collection<Franchise> franchises) {
        if ( franchises == null ) {
            return null;
        }

        Collection<FranchiseDTO> collection = new ArrayList<FranchiseDTO>( franchises.size() );
        for ( Franchise franchise : franchises ) {
            collection.add( franchiseToFranchiseDto( franchise ) );
        }

        return collection;
    }

    @Override
    public Franchise franchisePostDtoToFranchise(FranchisePostDTO franchisePostDTO) {
        if ( franchisePostDTO == null ) {
            return null;
        }

        Franchise franchise = new Franchise();

        franchise.setName( franchisePostDTO.getName() );

        return franchise;
    }
}
